'''
EMACC.PY

selenium
└───python
    └───emacc
        └───src
'''

# Imports
# -- for random details
import string
import random
# -- for selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
# -- for logging accounts
import datetime
# -- for waiting..
from time import sleep
# -- for config
import configparser

# quick stuff
today = datetime.datetime.today()

# get config
config = configparser.ConfigParser()
config.read("..\config.ini")
group_Members = config["emacc"]["group_Members"].split("\n")


# Driver startup
driver = webdriver.Chrome()
driver.get('https://gaggle.email/#create')

# Generate random details
details_Email = 'a'.join([random.choice(string.ascii_letters + string.digits) for _ in range(12)])
details_Pass = 'j'.join([random.choice(string.ascii_letters + string.digits) for _ in range(12)])
details_Group = 'g'.join([random.choice(string.ascii_letters + string.digits) for _ in range(12)])

# Input details
driver.find_element_by_xpath('//*[@id="create-group-email"]').send_keys(details_Email + '@o.com')
driver.find_element_by_xpath('//*[@id="create-group-password"]').send_keys(details_Pass)
driver.find_element_by_xpath('//*[@id="group-name-input"]').send_keys(details_Group)
driver.find_element_by_xpath('//*[@id="create-group-button"]').click()
driver.find_element_by_xpath('//*[@id="create-group-button"]').click()
sleep(5) # -- make sure registration is finished & popup has loaded

# Add email to group
driver.find_element_by_xpath('//*[@id="start-modal-close"]').click() # -- close popup

# -- find which add-members button is clickable
try:
    driver.find_element_by_xpath('//*[@id="member-cards-stat-add-members"]').click()
    member_Click = 0
except:
    driver.find_element_by_xpath('//*[@id="member-cards-blank-slate-add-members"]').click()
    member_Click = 1
sleep(1) # -- wait one second for animation

for group_Member in group_Members:
    driver.find_element_by_xpath('//*[@id="add-members-textarea"]').send_keys(group_Member + Keys.ENTER)
driver.execute_script("window.scrollTo(0, document.body.scrollHeight);") # -- scroll to buttom of page so button is clickable
driver.find_element_by_xpath('//*[@id="add-all-members-button"]').click() # -- add members

# log email account, password, group and other info in numbers
log = open('account_log.log', 'a')
log.write('| ' + str(today.year) + '/' + str(today.month) + '/' + str(today.day) + ' | ' + details_Email + '@o.com | ' + details_Pass + ' | ' + details_Group + '@gaggle.email | ' + str(member_Click) + ' |')
log.close()

# Sign out
driver.find_element_by_xpath('//*[@id="nav-mobile"]/li/a').click()
driver.find_element_by_xpath('//*[@id="desktop-logout"]/li[6]/a').click()
